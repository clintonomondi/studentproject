<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//common
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/proceed', 'HomeController@proceed')->name('proceed');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile', 'HomeController@updateaccount')->name('updateaccount');
Route::post('/password', 'HomeController@updatepass')->name('updatepass');


//admin/student
Route::post('add/student', 'AdminController@poststudent')->name('admin.poststudent');
Route::get('/students', 'AdminController@students')->name('admin.students');
Route::get('/students/remove/{id}', 'AdminController@removeStudent')->name('admin.removeStudent');
Route::get('/students/edit/{id}', 'HomeController@editstudent')->name('admin.editstudent');
Route::get('admin/read/{id}', 'AdminController@read')->name('admin.read');
Route::get('admin/assign/{id}', 'AdminController@assign')->name('admin.assign');
Route::post('admin/assign', 'AdminController@postAssign')->name('admin.postAssign');
Route::get('/users', 'AdminController@users')->name('admin.users');
Route::get('/student/all', 'AdminController@student')->name('students');
Route::get('/admin/projects', 'AdminController@projects')->name('admin.projects');
Route::get('/admin/more/{id}', 'AdminController@more')->name('admin.more');
Route::post('/admin/deadline', 'AdminController@postdeadline')->name('postdeadline');
//admin/lecturers
Route::post('add/lec', 'AdminController@postlec')->name('admin.postlec');
Route::get('/lecs', 'AdminController@lecs')->name('admin.lecs');
Route::get('/sups', 'AdminController@sups')->name('admin.sups');
Route::get('/admin/comments', 'AdminController@comments')->name('admin.comments');


//students
Route::get('student/projects', 'StudentController@projects')->name('student.projects');
Route::post('student/projects', 'StudentController@postProject')->name('student.postProject');
Route::get('student/add/project', 'StudentController@addproject')->name('student.addproject');
Route::post('student/projects/edit', 'StudentController@ediProject')->name('student.ediProject');
Route::post('student/upload', 'StudentController@upload')->name('student.upload');
Route::get('upload/delete/{id}', 'StudentController@cancel')->name('student.cancel');
Route::get('upload/read/{id}', 'StudentController@read')->name('student.read');
Route::get('comment/print', 'StudentController@printcomment')->name('student.printcomment');

//Lecturers
Route::get('my/students', 'LecController@students')->name('lec.students');
Route::get('my/student/{id}', 'LecController@moreStudent')->name('lec.moreStudent');
Route::get('my/comment/{id}', 'LecController@comment')->name('lec.comment');
Route::post('my/comment', 'LecController@postComment')->name('lec.postComment');

