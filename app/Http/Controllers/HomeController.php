<?php

namespace App\Http\Controllers;

use App\Deadline;
use App\File;
use App\Lec;
use App\Register;
use App\User;
use Illuminate\Http\Request;
use App\Student;
use App\Project;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use test\Mockery\ReturnTypeObjectTypeHint;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project=Project::where('user_id',Auth::user()->id)->first();
        $deadline=Deadline::find(1);
        return view('home',compact('project','deadline'));
    }

    public  function proceed(Request $request){
        $this->validate($request, [
            'identity' => 'required|unique:users',
        ]);
        $identity=$request->input('identity');
        $check=Register::where('identity',$identity)->first();
        if(!$check){
            return redirect()->back()->with('error','No such user in the system');
        }
        $identity2=$check->identity;
        $role=$check->description;
        $user=User::find(Auth::user()->id);
        $user->identity=$identity2;
        $user->role=$role;
        $user->save();

        if(($user->role)=='student'){
            $project=Project::create($request->all());
        }
        return redirect()->back()->with('success','Your application is approved successfully,refresh');

    }


    public  function updatepass(Request $request){
        $this->validate($request, [
            'currentpass' => 'required|min:5',
            'newpass' => 'required|min:5',
            'repass' => 'required|min:5',
        ]);
        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return redirect()->back()->with('error', 'Sorry the entered current password is wrong.');
        } elseif ($request->input('newpass') !== $request->input('repass')) {
            return redirect()->back()->with('error', 'Sorry the new passwords don\'t match.');
        }
        $id = auth()->user()->id;
        $currentUser = User::findOrFail($id);
        $currentUser->password = Hash::make($request->input('newpass'));
        $currentUser->save();
        return redirect()->back()->with('success','Password changed successfully');
    }


    public  function profile(){
        return view('profile');
    }

    public  function  updateaccount(Request $request){
        $name=$request->input('name');
        $email=$request->input('email');
        $user=User::find(Auth::user()->id);
        $user->name=$name;
        $user->email=$email;
        $user->save();
        return redirect()->back()->with('success','Information updated successfully');
    }


}
