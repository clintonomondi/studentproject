<?php

namespace App\Http\Controllers;

use App\Deadline;
use App\File;
use App\Project;
use App\Register;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function students(){
        $students=Register::where('description','Student')->orderBy('id','asc')->paginate(10);
        return view('admin.viewstudents',compact('students'));
    }

    public  function poststudent(Request $request){
        $request->validate([
            'identity'=>'required|unique:registers',
        ]);
        $student=Register::create($request->all());
        return redirect()->back()->with('success','New student added successfully');
    }

    public  function removeStudent($id){
        $student=Register::find($id);
        $student->delete();
        return redirect()->back()->with('success','Student Removed successfully');
    }

    public  function  lecs(){
        $lecs=Register::where('description','lec')->orderBy('id','asc')->paginate(10);
        return view('admin.viewlecs',compact('lecs'));
    }

    public  function users(){
        $users=User::orderBy('role','desc')->paginate(10);
        return view('admin.users',compact('users'));
    }

    public  function sups(){
        $users=User::where('role','lec')->paginate(10);
        return view('admin.lecs',compact('users'));
    }

    public  function student(){
        $users=User::where('role','student')->paginate(10);
        return view('admin.students',compact('users'));
    }

    public  function read($id){
        $project=Project::find($id);
        $files = Project::find($id)->file;
        $today=date('Y-m-d');
        return view('admin.read', compact('project', 'files','today'));
    }

    public  function projects(){
        $projects=Project::orderBy('id','asc')->paginate(10);
        $today=date('Y-m-d');
        return view('admin.projects',compact('projects','today'));
    }

    public  function assign($id){
        $user=User::find($id);
        $lecs=User::where('role','lec')->get();
        return view('admin.assign',compact('user','lecs'));
    }

    public  function postAssign(Request $request){
        $id=$request->input('id');
        $lec_id=$request->input('lec_id');
        $user=User::find($id);
        $user->lec_id=$lec_id;
        $user->save();
        return redirect()->back()->with('success','Student assigned supervisor successfully');
    }

    public  function more($id){
        $files=File::find($id);
        return view('admin.more',compact('files'));
    }

    public  function comments(){
        $comments=File::orderBy('status','asc')->paginate(10);
        return view('admin.comments',compact('comments'));
    }

    public  function postdeadline(Request $request){
        $date_ended=$request->input('date_ended');
        $projects=Project::all();
        foreach ( $projects as $project) {
            $project->date_ended=$date_ended;
            $project->save();
        }
        $deadline=Deadline::find(1);
        $deadline->date=$date_ended;
        $deadline->save();
        return redirect()->back()->with('success','Deadline date set successfully to all students');

    }
}
