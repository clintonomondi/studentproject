<?php

namespace App\Http\Controllers;

use App\Deadline;
use App\File;
use App\Project;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function addproject(){
        $deadline=Deadline::find(1);
        return view('student.addproject',compact('deadline'));
    }

    public  function projects(){
        $project=User::find(Auth::user()->id)->project;
        $today=date('Y-m-d');
        if($project==null){
            return redirect()->back()->with('error','Please Create a project');
        }
        $files = Project::findorFail($project->id)->file;
        return view('student.project', compact('project', 'files','today'));
    }

    public  function postProject(Request $request){
        $student=Project::where('user_id',Auth::user()->id)->first();
        if($student){
return redirect()->back()->with('error','You have already created project');
        }
        $project=Project::create($request->all());
        return redirect()->back()->with('success','Project Created');
    }

    public  function ediProject(Request $request){
        $id=$request->input('id');
        $description=$request->input('description');
        $title=$request->input('title');
        $project=Project::find($id);
        $project->title=$title;
        $project->description=$description;
        $project->save();
        return redirect()->back()->with('success','Project Updated');
    }

    public function upload(Request $request) {
        $project_id = $request->input('project_id');
        $request->validate([
            'filename' => 'required|mimes:pdf,docx|max:1999',
        ]);
        //hand file upload
        if ($request->hasFile('filename')) {
//ge filename with the extension
            $filenameWithExt = $request->file('filename')->getClientOriginalName();
            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get just extension
            $extension = $request->file('filename')->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            //upload the image
            $path = $request->file('filename')->storeAs('public/project_files', $fileNameToStore);

            File::create([
                'filename' => $fileNameToStore,
                'project_id' => $project_id,
                'location' => 'storage/project_files/' . $fileNameToStore,
                'path'=>$path,
            ]);
        } else {
            return redirect()->back()->with('error', 'please select a file');
        }

        return redirect()->back()->with('success', 'File uploaded successfully');
    }

    public function cancel($id) {
        $file = File::find($id);
        if ($file->status!='Pending'){
            return redirect()->back()->with('error','you cannot cancel this post');
        }
        Storage::delete($file->path);
        $file->delete();
        return redirect()->back()->with('success', 'File deleted successfully');
    }

    public  function read($id){
        $project=User::find(Auth::user()->id)->project;
        $files = File::find($id);
        return view('student.read', compact('project', 'files'));
    }

public  function  printcomment(){
 $proj=User::find(Auth::user()->id)->project;
 $commments=File::where('project_id',$proj->id)->orderBy('status','desc')->get();
 return view('student.printcomment',compact('commments','proj'));

}
}
