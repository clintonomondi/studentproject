<?php

namespace App\Http\Controllers;

use App\Lec;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Student;
use App\Project;
use App\File;
use Illuminate\Support\Facades\Hash;

class LecController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function students(){
        $users=User::where('lec_id',Auth::user()->name)->paginate(10);
        $today=date('Y-m-d');
        return view('lec.students',compact('users','today'));
    }

    public  function moreStudent($id){
        $project=Project::find($id);
        $files = Project::find($id)->file;
        $today=date('Y-m-d');
        return view('lec.moreStudent', compact('project', 'files','today'));
    }

    public  function comment($id){
        $file = File::find($id);
        return view('lec.comment', compact( 'file'));
    }
    public  function postComment(Request $request){
        $id=$request->input('id');
        $comment=$request->input('comment');
        $status=$request->input('status');
        $file=File::find($id);
        $file->comment=$comment;
        $file->status=$status;
        $file->save();
        return redirect()->back()->with('success','Comment submitted successfully');

    }

    public  function projects(){
        $id=Auth::user()->id;
        $students=Lec::find($id)->students;
        foreach ($students as $student){
            $files=$student->project;
            return $files;
        }

    }


}
