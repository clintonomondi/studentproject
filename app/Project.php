<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected  $fillable=['title','description','user_id','date_ended'];

    public  function user(){
        return $this->belongsTo(User::class);
    }

    public function file() {
        return $this->hasMany(File::class);
    }
}
