<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Miriam Jep',
            'email' =>'admin@gmail.com',
            'role' =>'admin',
            'identity' =>'admin',
            'password' => bcrypt('admin'),
        ]);
    }
}
