<?php

use Illuminate\Database\Seeder;

class DeadlineSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deadlines')->insert([
            'date' => '2019-05-22',

        ]);
    }
}
