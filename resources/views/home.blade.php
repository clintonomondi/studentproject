@extends('layouts.app')

@section('content')
    @include('includes.message')
                <div class="row justify-content-center">
                    <div class="col-sm-10">
                @can('isAdmin')
                <h1 class="mt-4">DashBoard: Admin</h1>
                <img src="{{asset('images/admin.jpg')}}" alt="Admin avarter">
                        <div class="alert alert-success">
                            Welcome {{Auth::user()->name}} to Chuka University Project management System.
                        </div>
@endcan
                        @can('isStudent')
                        <h1 class="mt-4">DashBoard: Student</h1>
                        @if(count($project)>0)
                            <form method="post" action="{{route('student.ediProject')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Title:</label>
                                    <input type="text" class="form-control" name="title" value=" {{$project->title}} " required>
                                </div>
                                <input type="text" name="id" value="{{$project->id}}" hidden>
                                <div class="form-group">
                                    <label for="comment">Description:</label>
                                    <textarea class="form-control" rows="5" name="description" required>{{$project->description}}</textarea>
                                </div>
                        <button type="submit" class="btn btn-outline-info btn-sm">Update</button>
                    </form>
                            @endif
                            @endcan


                        @can('isLec')
                        <h1 class="mt-4">DashBoard: Supervisor</h1>
                        <img src="{{asset('images/lec.png')}}" alt="Admin avarter">
                        <div class="alert alert-success">
                            Welcome {{Auth::user()->name}} to Chuka University Project management System.
                        </div>
                            @endcan


                        @can('isUnassigned')
                            <p style="color: red">Please proceed with application</p>
                        <form method="post" action="{{route('proceed')}}">
                            @csrf
                            <div class="form-group">
                                <label for="email">Enter Regno/Jobid:</label>
                                <input type="text" class="form-control" name="identity" required>
                                <input type="text" class="form-control" name="user_id" value="{{Auth::user()->id}}" hidden>
                                <input type="text" name="date_ended" value="{{$deadline->date}}" hidden>
                            </div>

                            <div class="form-group form-check">
                            </div>
                            <button type="submit" class="btn btn-outline-info btn-sm">Proceed</button>
                        </form>

                    @endcan

            </div>
                </div>
@endsection
