<!-- The Modal -->
<div class="modal fade" id="editaccount">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">Edit account</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form role="form" method="POST" action="{{route('updateaccount')}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Name:</label>
                        <input type="text" class="form-control" value="{{Auth::user()->name}}" name="name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control"value="{{Auth::user()->email}}" name="email">
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-info btn-sm">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
