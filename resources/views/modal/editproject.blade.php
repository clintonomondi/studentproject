<!-- The Modal -->
<div class="modal fade" id="editProject">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">Edit Project</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                @if(count($project)>0)
                <form method="post" action="{{route('student.ediProject')}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Title:</label>
                        <input type="text" class="form-control" name="title" value=" {{$project->title}} " required>
                    </div>
                    <input type="text" name="id" value="{{$project->id}}" hidden>
                    <div class="form-group">
                        <label for="comment">Description:</label>
                        <textarea class="form-control" rows="5" name="description" required>{{$project->description}}</textarea>
                    </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-info btn-sm">Update</button>
            </div>
            </form>
            @endif
        </div>
    </div>
</div>
