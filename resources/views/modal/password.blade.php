<!-- The Modal -->
<div class="modal fade" id="password">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form role="form" method="POST" action="{{route('updatepass')}}">
                   @csrf
                    <div class="form-group">
                        <label for="email">Current password:</label>
                        <input type="password" class="form-control" id="password" name="currentpass">
                    </div>
                    <div class="form-group">
                        <label for="email">New Password</label>
                        <input type="password" class="form-control" id="password" name="newpass">
                    </div>
                    <div class="form-group">
                        <label for="email">Confirm password:</label>
                        <input type="password" class="form-control" id="password" name="repass">
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-info btn-sm">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
