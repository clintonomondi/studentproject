<!-- The Modal -->
<div class="modal fade" id="assignSupervisior">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">Assign Supervisor</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="post" action="{{route('admin.postAssign')}}">
                    @csrf
                    <div class="form-group">
                        <label for="sel1">Select Supervisor:</label>
                        <select class="form-control" name="lec_id" required>
                            <option>{{$user->lec_id}}</option>
                            @foreach($lecs as $lec)
                                <option value="{{$lec->name}}">{{$lec->name}}</option>
                                @endforeach
                        </select>
                        <input type="text" name="id" value="{{$user->id}}" hidden>
                    </div>
                    <button type="submit" class="btn btn-outline-info btn-sm">Submit</button>
                </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
            </div>
            </form>
        </div>
    </div>
</div>
