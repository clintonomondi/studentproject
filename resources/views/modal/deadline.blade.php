<!-- The Modal -->
<div class="modal fade" id="deadline">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header bg-info">
                <h4 class="modal-title">Set deadline date</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form role="form" method="POST" action="{{route('postdeadline')}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Deadline date:</label>
                        <input type="date" class="form-control" name="date_ended" required>
                    </div>

            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-info btn-sm">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
