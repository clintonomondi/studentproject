@if(count($errors)>0)
    @foreach($errors->all()  as $error)
        <div id="BodyField">
            <div class="video-field-new">
                <div class="alert alert-danger">
                    {{$error}}
                </div>
            </div>
        </div>
    @endforeach
@endif

@if(session('success'))
    <div id="BodyField">
        <div class="video-field-new">
            <div class="alert alert-success">
                {{session('success')}}
             </div>
        </div>
    </div>
@endif
@if(session('error'))
    <div id="BodyField">
        <div class="video-field-new">
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        </div>
    </div>
@endif

<script>
    $(function() {
        $('.video-field-new').delay(8500).show().fadeOut('slow');
    });
</script>
