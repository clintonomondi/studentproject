@extends('layouts.commonbar')

@section('content')
    <div class="container-fluid" style="margin-top:40px">
        <div class="row justify-content-center">
            <div class="col-sm-4">
                <h4>Lecturer Account</h4><hr>
                @include('includes.message')
                <form method="post" action="{{route('lecPostAccount')}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Job ID:</label>
                        <input type="text" class="form-control form-control-sm"  name="jobid" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Name:</label>
                        <input type="text" class="form-control form-control-sm"  name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Email:</label>
                        <input type="email" class="form-control form-control-sm"  name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control form-control-sm"  name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Confirm password:</label>
                        <input type="password" class="form-control form-control-sm"  name="repass" required>
                    </div>
                    <button type="submit" class="btn btn-outline-info btn-sm">Register</button>
                    <a class="btn btn-link" href="{{route('lecLogin')}}">Login</a>
                </form>


            </div>
        </div>
    </div>

@endsection
