@extends('layouts.commonbar')

@section('content')
    <div class="container-fluid" style="margin-top:40px">
        <div class="row justify-content-center">
            <div class="col-sm-4">
                <h4>Lecturer Login</h4><hr>
                @include('includes.message')
                <form method="post" action="{{route('lecPostLogin')}}">
                    @csrf
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" class="form-control form-control-sm" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control form-control-sm" name="password">
                    </div>
                    <div class="form-group form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> Remember me
                        </label>
                    </div>
                    <button type="submit" class="btn btn-outline-info btn-sm">Login</button>
                    <a class="btn btn-link" href="{{route('lecAccount')}}">Create account</a>
                </form>


            </div>
        </div>
    </div>

@endsection
