@extends('layouts.app')

@section('content')
    @include('includes.message')
    <div class="row justify-content-center">
        <div class="col-sm-10">
            <form method="post" action="{{route('student.postProject')}}">
                @csrf
                <div class="form-group">
                    <label for="email">Title:</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <input type="text" name="user_id" value="{{Auth::user()->id}}" hidden>
                <input type="text" name="date_ended" value="{{$deadline->date}}" hidden>
                <div class="form-group">
                    <label for="comment">Description:</label>
                    <textarea class="form-control" rows="5" name="description" required></textarea>
                </div>
            <button type="submit" class="btn btn-outline-info btn-sm">Submit</button>
        </form>
        </div>
    </div>
@endsection
