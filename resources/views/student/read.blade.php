@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.editproject')
    <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-6">
                                <strong>Supervisor:</strong>{{Auth::user()->lec_id}}
                                    <div class="card border">
                                        <div class="card-header">{{$project->title}}
                                            <span class="float-right" style="color: green;">{{$project->status}}</span>
                                        </div>
                                        <div class="card-body">
                                            <p> {{$project->description}}</p>
                                        </div>
                                        <a href="#" data-toggle="modal" data-target="#editProject">
                                            <div class="card-footer">
                                                <span class="pull-left fa fa-edit">Edit</span>
                                            </div>
                                        </a>
                                    </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-header">Comment:::
                                            <a href="{{asset($files->location)}}" download>{{$files->filename}}</a>
                                        <span class="float-right">{{$files->updated_at}}</span>
                                    </div>
                                    <div class="card-body">
                                           <p>{{$files->comment}}</p>
                                    </div>
                                    <div class="card-footer">
                                            <span class="pull-left">
<a href="{{route('student.projects')}}" class="fa fa-backward">Back</a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
