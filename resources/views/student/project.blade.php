@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.editproject')
        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-6">
                                <strong>Supervisor:</strong>{{Auth::user()->lec_id}}
                                <div class="card border">
                                    <div class="card-header">{{$project->title}}
                                        @if(($project->date_ended)>=$today)
                                            <span class="float-right" style="color: green;">Active-{{$project->date_ended}}</span>
                                        @else
                                            <span class="float-right" style="color: red;">Closed</span>
                                        @endif
                                    </div>
                                <div class="card-body">
                                    <p> {{$project->description}}</p>
                                </div>
                                    <a href="#" data-toggle="modal" data-target="#editProject">
                                        <div class="card-footer">
                                            <span class="pull-left fa fa-edit">Edit</span>
                                        </div>
                                    </a>
                            </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-header">Project Files</div>
                                <div class="card-body">
                                    @if(count($files)>0)
                                        <table class="table table-bordered">
                                            <thead>
                                            <th>Project</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th></th>
                                            </thead>
                                            <tbody id="myTable">
                                            @foreach($files as $key=> $file)
                                                <tr>
                                                    <td>
                                                        <a href="{{asset($file->location)}}" download>{{$file->filename}}</a>
                                                    </td>
                                                    <td style="color: red;">{{$file->created_at}}</td>
                                                    <td><a class=" fa fa-edit" style="color: red"
                                                           href="{{route('student.cancel',['id'=>$file->id])}}">Cancel</a>
                                                    </td>
                                                    @if(($file->status)=='Pending')
                                                    <td><a class=" fa fa-envelope" style="color: green"
                                                           href="{{route('student.read',['id'=>$file->id])}}">{{$file->status}}</a>
                                                    </td>
                                                        @else
                                                        <td><a class=" fa fa-envelope" style="color: sandybrown"
                                                               href="{{route('student.read',['id'=>$file->id])}}">{{$file->status}}</a>
                                                        </td>
                                                        @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <p>No upload records yet</p>
                                    @endif
                                </div>
                                        <div class="card-footer">
                                            @if(($project->date_ended)>=$today)
                                            <span class="pull-left">Upload file
                                                <form method="post" action="{{route('student.upload')}}"
                                                      enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="file" name="filename">
                                                    <input type="text" name="project_id" value="{{$project->id}}" hidden>
                                                    <button type="submit" class="btn btn-outline-info btn-sm fa fa-upload">Upload</button>
                                                </form>
                                            </span>
                                            @else
                                                <p style="color: red">Deadline reached</p>
                                                @endif
                                        </div>
                            </div>
                            </div>
                            </div>
@endsection
