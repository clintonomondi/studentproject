@extends('layouts.app')

@section('content')
        @include('includes.message');
        <div class="row justify-content-center">
            <div class="col-sm-12 col-sm-12">
                                    <div class="table-responsive">
                                        <div id="tab">
                                            <h1>{{$proj->title}}</h1>

                                            <br/>
                                            <table width="100%" class="table table-striped" >
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="text-center">File name</th>
                                                    <th class="text-center">Status</td>
                                                    <th class="text-center">Uploaded at</td>
                                                    <th class="text-center">Commented at</td>
                                                    <th class="text-center">Comment</td>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($commments)>0)
                                                    @foreach($commments as $key=>$commment)
                                                        <tr >
                                                            <td>{{$key+1}}</td>
                                                            <td>  <a href="{{asset($commment->location)}}" download>{{$commment->filename}}</a></td>
                                                            <td>{{$commment->status}}</td>
                                                            <td>{{$commment->created_at}}</td>
                                                            <td>{{$commment->updated_at}}</td>
                                                            <td>{{$commment->comment}}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <p>No record</p>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" class="btn btn-deafult btn-sm"
                                                id="btPrint" onclick="createPDF()" />
                                        <i class="fa fa-print  fa-fw"></i> Print
                                        </button>
                                    </div>

                                </div>
                                <!-- /.col-lg-12 -->

                    @endsection
                    <script>
                        function createPDF() {
                            var sTable = document.getElementById('tab').innerHTML;

                            var style = "<style>";
                            style = style + "table {width: 100%;font: 17px Calibri;}";
                            style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
                            style = style + "padding: 2px 3px;text-align: left;}";
                            style = style + "</style>";

                            // CREATE A WINDOW OBJECT.
                            var win = window.open('', '', 'height=700,width=700');

                            win.document.write('<html><head>');
                            win.document.write('<title> {{$proj->title}}</title>');   // <title> FOR PDF HEADER.
                            win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
                            win.document.write('</head>');
                            win.document.write('<body>');
                            win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
                            win.document.write('</body></html>');

                            win.document.close(); 	// CLOSE THE CURRENT WINDOW.

                            win.print();    // PRINT THE CONTENTS.
                        }
                    </script>
