@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.addstudent')
        <div class="row justify-content-center">
            <div class="col-sm-10">
                <span class="float-left">Students</span>
                <span class="float-right"><a class="btn btn-outline-info btn-sm"  data-toggle="modal" data-target="#addstudent" >Add</a></span>
                      <table class="table  table-striped table-hover">
                         <thead>
                         <th>#</th>
                         <th>Identity</th>
                         <th>Description</th>
                         <th></th>
                         </thead>
                         <tbody id="myTable">
                         @if(count($students)>0)
                             @foreach($students as $key =>$student)
                                 <tr>
                                     <td>{{$key+1}}</td>
                                     <td>{{$student->identity}}</td>
                                     <td>{{$student->description}}</td>
                                     <td><a class="fa fa-trash" style="color: red" href="{{route('admin.removeStudent',['id'=>$student->id])}}">Remove</a> </td>
                                 </tr>
                                 @endforeach
                             @endif
                         </tbody>
                     </table>
                      {{$students->links()}}
                    </div>
                </div>
@endsection
