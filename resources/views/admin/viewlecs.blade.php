@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.addlec')
    <div class="row justify-content-center">
        <div class="col-sm-10">
            <span class="float-left">Supervisiors</span>
            <span class="float-right"><a class="btn btn-outline-info btn-sm"  data-toggle="modal" data-target="#addlec" >Add</a></span>
            <table class="table  table-striped table-hover">
                <thead>
                <th>#</th>
                <th>Identity</th>
                <th>Description</th>
                <th></th>
                </thead>
                <tbody id="myTable">
                @if(count($lecs)>0)
                    @foreach($lecs as $key =>$lec)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$lec->identity}}</td>
                            <td>{{$lec->description}}</td>
                            <td><a class="fa fa-trash" style="color: red" href="{{route('admin.removeStudent',['id'=>$lec->id])}}">Remove</a> </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{$lecs->links()}}
        </div>
    </div>
@endsection
