@extends('layouts.app')

@section('content')
    @include('includes.message')
    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-6">
            <div class="card ">
                <div class="card-header">{{$files->project->title}}
                    <span class="float-right" style="color: green;">{{$files->project->status}}</span>
                </div>
                <div class="card-body">
                    <p> {{$files->project->description}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="card">
                <div class="card-header">Comment:::
                    <a href="{{asset($files->location)}}" download>{{$files->filename}}</a>
                    <span class="float-right">{{$files->updated_at}}</span>
                </div>
                <div class="card-body">
                    <p>{{$files->comment}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
