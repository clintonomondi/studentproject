@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="margin-top:40px">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="card border-info">
                    <div class="card-header">{{$lec->name}}->Students</div>
                    <div class="card-body">
                        @include('includes.message')
                        <table class="table  table-responsive-sm table-bordered table-hover">
                            <thead>
                            <th>#</th>
                            <th>Regno</th>
                            <th>Name</th>
                            <th>Course</th>
                            <th></th>
                            </thead>
                            <tbody id="myTable">
                            @if(count($students)>0)
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{$student->id}}</td>
                                        <td>{{$student->regno}}</td>
                                        <td>{{$student->name}}</td>
                                        <td>{{$student->course}}</td>
                                        <td><a class="fa fa-edit" style="color: blue;" href="{{route('admin.editstudent',['id'=>$student->id])}}">edit</a> </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
