@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.addlec')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" href="{{route('admin.users')}}">All users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('students')}}">Students</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin.sups')}}">Supervisiors</a>
                </li>
            </ul>
            <table class="table  table-striped table-hover">
                <thead>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Identity</th>
                <th>Role</th>
                </thead>
                <tbody id="myTable">
                @if(count($users)>0)
                    @foreach($users as $key =>$user)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->identity}}</td>
                            @if(($user->role)=='Unassigned')
                            <td style="color: red">{{$user->role}}</td>
                            @else
                                <td>{{$user->role}}</td>
                            @endif
                            {{--<td><a class="fa fa-trash" style="color: red" href="{{route('admin.removeStudent',['id'=>$lec->id])}}">Remove</a> </td>--}}
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{$users->links()}}
        </div>
    </div>
@endsection
