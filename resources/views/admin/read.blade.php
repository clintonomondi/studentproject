@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.editproject')
    <div class="row justify-content-center">
        <div class="col-md-6 col-sm-6">
          <span class="fa float-left"> <strong>Supervisor:</strong>{{$project->user->lec_id}}</span><hr>
            <div class="card border">
                <div class="card-header">{{$project->title}}
                    @if(($project->date_ended)>=$today)
                        <span class="float-right" style="color: green;">Active-{{$project->date_ended}}</span>
                    @else
                        <span class="float-right" style="color: red;">Closed</span>
                    @endif
                </div>
                <div class="card-body">
                    <p> {{$project->description}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="card">
                <div class="card-header">{{$project->user->name}}</div>
                <div class="card-body">
                    @if(count($files)>0)
                        <table class="table table-bordered">
                            <thead>
                            <th>Project</th>
                            <th>Date</th>
                            <th>Status</th>
                            </thead>
                            <tbody id="myTable">
                            @foreach($files as $key=> $file)
                                <tr>
                                    <td>
                                        <a href="{{asset($file->location)}}" download>{{$file->filename}}</a>
                                    </td>
                                    <td style="color: red;">{{$file->created_at}}</td>
                                    @if(($file->status)=='Pending')
                                        <td><a class=" fa fa-envelope" style="color: green"
                                               href="{{route('admin.more',['id'=>$file->id])}}">{{$file->status}}</a>
                                        </td>
                                    @else
                                        <td><a class=" fa fa-envelope" style="color: sandybrown"
                                               href="{{route('admin.more',['id'=>$file->id])}}">{{$file->status}}</a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>No upload records yet</p>
                    @endif
                </div>

            </div>
        </div>
    </div>
@endsection
