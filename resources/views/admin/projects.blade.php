@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.deadline')
    <span class="fa fa-pull-right"><a class="btn btn-info btn-sm"  data-toggle="modal" data-target="#deadline">Set deadline</a> </span>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <table class="table  table-striped">
                <thead>
                <th>#</th>
                <th>Title</th>
                <th>Student</th>
                <th>Supervisor</th>
                <th>Status</th>
                </thead>
                <tbody id="myTable">
                @if(count($projects)>0)
                    @foreach($projects as $key =>$project)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td><a  href="{{route('admin.read',$project->id)}}">{{$project->title}}</a> </td>
                            <td>{{$project->user->name}}</td>
                            @if(($project->user->lec_id)==null)
                                <td><a class="btn btn-primary btn-sm" href="{{route('admin.assign',$project->user->id)}}">Assign</a></td>
                               @else
                                <td> <a class="btn btn-success btn-sm" href="{{route('admin.assign',$project->user->id)}}">{{$project->user->lec_id}}</a></td>
                            @endif
                            @if(($project->date_ended)>=$today)
                                <td><a class="btn btn-success btn-sm">Active-{{$project->date_ended}}</a> </td>
                            @else
                                <td><a class="btn btn-danger btn-sm">Closed</a> </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{$projects->links()}}
        </div>
    </div>
@endsection
