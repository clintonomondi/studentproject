@extends('layouts.app')

@section('content')
    @include('includes.message');
    {{$comments->links()}}
    <div class="container">
@if(count($comments)>0)
    @foreach($comments as $comment)
        <div class="row justify-content-center">
                <div class="col-sm-8 col-sm-8">
                <span class="fa fa-pull-left"> <h3><a href="{{route('admin.more',$comment->id)}}" style="color: black;">{{$comment->filename}}</a> </h3></span>
                <span class="fa fa-pull-right"> <h6>{{$comment->created_at->diffForHumans()}}</h6></span>
                <hr><br>
                <p>{{$comment->comment}}</p>
                @if(($comment->status)=='Pending')
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:50%">Pending</div>
                    </div>
                    <hr>
                    <div>
                        @elseif(($comment->status)=='Sorted')
                            <div class="progress">
                                <div class="progress-bar bg-success " style="width:100%">Sorted</div>
                            </div>
                            <span class="fa fa-pull-left"><h6>Commented on: {{$comment->updated_at->diffForHumans()}}</h6></span>
                        @endif

                    </div>
            </div>
        </div>
        <br/><br/><br/><br/><br/>
                @endforeach
@endif
    </div>
@endsection

