@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.assignSupervisior')
    <div class="row justify-content-center">
        <div class="col-sm-10">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{$user->name}}</h4>
                    <table class="table">
                        <tr>
                            <td>Email</td>
                            <td>{{$user->email}}</td>
                        </tr>
                        <tr>
                            <td>Reg no</td>
                            <td>{{$user->identity}}</td>
                        </tr>
                        <tr>
                            <td> Supervisor</td>
                            <td > {{$user->lec_id}}</td>

                        </tr>
                        <tr>
                            <td >Project title</td>
                            <td><a href="{{route('admin.read',$user->project->id)}}">{{$user->project->title}}</a></td>
                        </tr>

                        </tbody>
                    </table>
                    <a href="#" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#assignSupervisior">Assign</a>
                </div>
            </div>
        </div>
    </div>
@endsection
