@extends('layouts.app')

@section('content')
    @include('includes.message')
    @include('modal.password')
    @include('modal.editaccoiunt')
        <div class="row justify-content-center">
            <div class="col-md-10">
                    Personal information
                        <table class="table">
                            <thead>
                            <th></th>
                            <th></th>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{Auth::user()->name}}</td>
                            </tr>
                            <tr>
                                <td>Identitiy</td>
                                <td>{{Auth::user()->identity}}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{Auth::user()->email}}</td>
                            </tr>
                            <tr>
                                <td><a class="fa fa-edit" href="#"  data-toggle="modal" data-target="#editaccount">Edit</a> </td>
                                <td><a class="fa fa-key" href="#"  data-toggle="modal" data-target="#password">Change password</a> </td>
                            </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
@endsection
