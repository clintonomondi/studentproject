<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Chuka</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/JQuery-3.3.1.min.js')}}"></script>

</head>
<body>
<nav class="navbar navbar-expand-md bg-info navbar-dark fixed-top">
    <!-- Brand -->
    <a class="navbar-brand" href="#">
        <img src="{{asset('images/logo.jpg')}}" alt="Logo" style="width:40px;">Supervisor
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('lec.home')}}">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Students
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('lec.students')}}">My students</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Projects
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('lec.projects')}}">All Projects</a>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <form class="form-inline">
                <input class="form-control-sm mr-sm-2" type="text" placeholder="Search" id="myInput">
            </form>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('lec.profile')}}">Account</a><hr>
                    <a class="dropdown-item" href="{{route('lec.logout')}}">Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<main class="py-4">
    @yield('content')
</main>
<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
</body>
</html>
