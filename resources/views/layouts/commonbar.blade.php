<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Chuka</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('js/app.js')}}"></script>


</head>
<body>
<nav class="navbar navbar-expand-md bg-info navbar-dark">
    <!-- Brand -->
    <a class="navbar-brand" href="#">
        <img src="{{asset('images/logo.jpg')}}" alt="Logo" style="width:40px;">Chuka University
    </a>

    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Navbar links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="btn btn-success btn-sm" href="{{route('studentLogin')}}">Students</a>
            </li>&nbsp;&nbsp;&nbsp;
            <li class="nav-item">
                <a class="btn btn-primary btn-sm" href="{{route('lecLogin')}}">Lecturers</a>
            </li>&nbsp;
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('login')}}">Admin</a>
                        </li>
                    @endauth
                </div>
            @endif
        </ul>
    </div>
</nav>
<main class="py-4">
    @yield('content')
</main>
</body>
</html>
