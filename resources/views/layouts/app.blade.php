<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Chuka-Student</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    {{--<link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">--}}
    <link href="{{asset('css/fontawesome/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('bar/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('bar/css/simple-sidebar.css')}}" rel="stylesheet">
    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
    <script src="{{asset('js/JQuery-3.3.1.min.js')}}"></script>

</head>
<body>
<div class="d-flex" id="wrapper">
@guest
@else
    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading"><img src="{{asset('images/logo.jpg')}}" alt="Logo" style="width:40px;"> </div>
        <div class="list-group list-group-flush">
            <a href="{{route('home')}}" class="list-group-item list-group-item-action bg-light">Dashboard</a>
           {{--admin--}}
            @can('isAdmin')
            <a href="{{route('admin.users')}}" class="list-group-item list-group-item-action bg-light">Users</a>
            <a href="{{route('admin.students')}}" class="list-group-item list-group-item-action bg-light">Students</a>
            <a href="{{route('admin.lecs')}}" class="list-group-item list-group-item-action bg-light">Supervisors</a>
            <a href="{{route('admin.projects')}}" class="list-group-item list-group-item-action bg-light">Projects</a>
                <a href="{{route('admin.comments')}}" class="list-group-item list-group-item-action bg-light">Comments</a>
            @endcan
            {{--endadmin--}}
            {{--students--}}
            @can('isStudent')
                <a href="{{route('student.addproject')}}" class="list-group-item list-group-item-action bg-light">Add project</a>
                <a href="{{route('student.projects')}}" class="list-group-item list-group-item-action bg-light">My projects</a>
                <a href="{{route('student.printcomment')}}" class="list-group-item list-group-item-action bg-light">Comments</a>
                @endcan
            {{--endstudents--}}
            {{--lecturer--}}
            @can('isLec')
                <a href="{{route('lec.students')}}" class="list-group-item list-group-item-action bg-light">My Students</a>
                @endcan
            {{--lecturer--}}
            <a href="{{route('profile')}}" class="list-group-item list-group-item-action bg-light">Account</a>
            <a href="{{route('profile')}}" class="list-group-item list-group-item-action bg-light">Password</a>
        </div>
    </div>
@endguest
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-info border-bottom navbar-fixed-top">
            <button class="btn btn-info" id="menu-toggle"> Menu</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <form class="form-inline">
                        <input class="form-control form-control-sm" type="text" placeholder="Search" id="myInput">
                    </form>
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                    @else
                    <!-- Links -->

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <main class="py-4">
                @yield('content')
            </main>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<script>
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
<script src="{{asset('bar/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('bar/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
</body>
</html>
