@extends('layouts.app')

@section('content')
    @include('includes.message')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <table class="table  table-striped">
                <thead>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Regno</th>
                <th>Project Title</th>
                <th>Status</th>
                </thead>
                <tbody id="myTable">
                @if(count($users)>0)
                    @foreach($users as $key =>$user)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->identity}}</td>
                            <td><a  href="{{route('lec.moreStudent',$user->project->id)}}">{{$user->project->title}}</a> </td>
                            @if(($user->project->date_ended)>=$today)
                                <td><a class="btn btn-success btn-sm">Active-{{$user->project->date_ended}}</a> </td>
                            @else
                                <td><a class="btn btn-danger btn-sm">Closed</a> </td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            {{$users->links()}}
        </div>
    </div>
@endsection
