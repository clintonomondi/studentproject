@extends('layouts.lecbar')

@section('content')
    <div class="container-fluid" style="margin-top:60px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-info">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in! Lecturer
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
