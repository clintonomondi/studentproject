@extends('layouts.app')

@section('content')
    @include('includes.message')
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-6">
                                @if(count($project)>0)
                                    <div class="card border">
                                        <div class="card-header">{{$project->title}}
                                            @if(($project->date_ended)>=$today)
                                                <span class="float-right" style="color: green;">Active-{{$project->date_ended}}</span>
                                            @else
                                                <span class="float-right" style="color: red;">Closed</span>
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p> {{$project->description}}</p>
                                        </div>
                                    </div>
                            </div>
                            @else
                                <p>No project </p>
                            @endif
                            <div class="col-md-6 col-sm-6">
                                <div class="card">
                                    <div class="card-header">Project Files</div>
                                    <div class="card-body">
                                        @include('includes.message')
                                        @if(count($files)>0)
                                            <table class="table table-responsive-sm table-bordered">
                                                <thead>
                                                <th>Project</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                </thead>
                                                <tbody id="myTable">
                                                @foreach($files as $file)
                                                    <tr>
                                                        <td>
                                                            <a href="{{asset($file->location)}}" download>{{$file->filename}}</a>
                                                        </td>
                                                        <td>{{$file->created_at}}</td>
                                                        @if(($file->status)=='Pending')
                                                            <td><a class=" fa fa-envelope" style="color: green"
                                                                   href="{{route('lec.comment',['id'=>$file->id])}}">{{$file->status}}</a>
                                                            </td>
                                                        @else
                                                            <td><a class=" fa fa-envelope" style="color: sandybrown"
                                                                   href="{{route('lec.comment',['id'=>$file->id])}}">{{$file->status}}</a>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No upload records yet</p>
                                        @endif
                                    </div>
                                    <div class="card-footer">
                                            <span class="pull-left">
                                                <a class="fa fa-backward" href="{{route('lec.students')}}">Back</a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
@endsection
