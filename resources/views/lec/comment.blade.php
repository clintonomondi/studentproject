@extends('layouts.app')

@section('content')
    @include('includes.message')
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-6">
                                    <div class="card border">
                                        <div class="card-header">{{$file->project->title}}
                                            <span class="float-right" style="color: green;">{{$file->project->status}}</span>
                                        </div>
                                        <div class="card-body">
                                            <p> {{$file->project->description}}</p>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="card ">
                                    <div class="card-header">Write Comment:::
                                        <a href="{{asset($file->location)}}" download>{{$file->filename}}</a>
                                        <span class="float-right">{{$file->updated_at}}</span>
                                    </div>
                                    <div class="card-body">
                                        <form method="post" action="{{route('lec.postComment')}}">
                                            @csrf
                                        <div class="form-group">
                                            <label for="comment">Comment:</label>
                                            <textarea class="form-control" rows="5" name="comment">{{$file->comment}}</textarea>
                                            <input type="text" name="id" value="{{$file->id}}" hidden>
                                            <input type="text" name="status" value="Sorted" hidden>
                                        </div>
                                            <button type="submit" class="btn btn-outline-info btn-sm">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
@endsection
