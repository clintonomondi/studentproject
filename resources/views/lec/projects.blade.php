@extends('layouts.lecbar')

@section('content')
    <div class="container-fluid" style="margin-top:40px">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border-info">
                    <div class="card-header">Projects
                        <span class="float-right">
                            <a class="btn btn-outline-info btn-sm " data-toggle="modal" data-target="#assignSupervisior">Assign supervisor</a>
                        </span>
                    </div>

                    <div class="card-body">
                        @include('includes.message')
                                        @include('includes.message')
                                        @if(count($files)>0)
                                            <table class="table table-responsive-sm table-bordered">
                                                <thead>
                                                <th>Project</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th></th>
                                                <th></th>
                                                </thead>
                                                <tbody id="myTable">
                                                @foreach($files as $file)
                                                    <tr>
                                                       <td>{{$file->title}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <p>No upload records yet</p>
                                        @endif
                                    </div>
                                    <div class="card-footer">
                                            <span class="pull-left">
                                                <a class="fa fa-backward" href="{{route('lec.students')}}">Back</a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection
